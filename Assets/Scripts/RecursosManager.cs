using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class RecursosManager : MonoBehaviour
{
    public GameObject[] gameObject;
    public delegate void subirnivel();
    public subirnivel lvlup;
    double xp;
    double xpMax;
    int rupias;
    int madera;
    int piedra;
    int lvl;
    int piedraelemntal;
    public GameObject[] textos;
    int enemigos;
    void Start()
    {
        enemigos = 0;
        piedraelemntal = 0;
        madera = 5;
        piedra = 5;
        xp = 98;
        xpMax = 100;
        Debug.Log(this.madera);
        Debug.Log(this.piedra);
        Debug.Log(this.xp);
        StartCoroutine(spawner());
    }

    void Update()
    {
        if (xp >= xpMax)
        {
            levelup();
        }
        textos[0].GetComponent<TextMeshProUGUI>().text = "MADERA "+madera;
        textos[1].GetComponent<TextMeshProUGUI>().text = "PIEDRA "+piedra;
        textos[2].GetComponent<TextMeshProUGUI>().text = "Enemigos "+enemigos;
      
        
    }
    void levelup()
    {
        lvlup.Invoke();
        xp -=xpMax;
        xpMax *= 1.05;
        lvl++;
    }

    IEnumerator spawner()
    {
        while (enemigos>=0 && enemigos<=5)
        {
            yield return new WaitForSeconds(10f);
            GameObject en = Instantiate(gameObject[0]);
            en.GetComponent<Enemigo>().deathenemigo += dropEnemigo;
            en.transform.position= new Vector2(0,0);
            enemigos++;

        }   
    }

    void dropEnemigo()
    {
        if (gameObject[0].transform.tag == "Duende") {
            xp += 10;
            int rand = Random.Range(0, 3);
            madera += rand;
            piedra += 2;
        }else if (gameObject[1].transform.tag == "Flama")
        {
            xp += 15;
            int rand = Random.Range(0, 3);
            madera += rand;
            piedra += 2;
        }else if(gameObject[1].transform.tag == "Wata")
        {
            xp += 10;
            int rand = Random.Range(0, 3);
            madera += rand;
            piedra += 2;
        }
    }
}
