using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    // Start is called before the first frame update
    public int hp;
    public float speed;
    public delegate void muerteenemigo();
    public muerteenemigo deathenemigo;
 

    void Start()
    {
        
    }

    public virtual void morir()
    {
        if (hp <= 0)
        {
            Debug.Log("MUELTO");
            Destroy(gameObject);
            deathenemigo.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    /*virtual OnCollisionEnter2D(Collision2D coll)
    {

    }*/
}
