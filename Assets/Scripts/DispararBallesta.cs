using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispararBallesta : MonoBehaviour
{
    GameObject projectil;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemic")
        {
            print("Colision" + collision.transform.name);
            StartCoroutine(disparar());
        }
    }
    public IEnumerator disparar()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            GameObject proj = Instantiate(projectil);
            GameObject enemic = GameObject.FindWithTag("Enemic");
            Vector2 direccio = enemic.transform.position - transform.parent.position;
            direccio.Normalize();
            proj.transform.up = direccio;
            proj.transform.position = this.transform.parent.position;
            proj.GetComponent<Rigidbody2D>().velocity = direccio * 10;
        }
    }
}
