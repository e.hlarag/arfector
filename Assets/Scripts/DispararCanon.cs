using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispararCanon : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject projectil;
    bool disparando;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Duende")
        {
            print("Colision" + collision.transform.name);
            StartCoroutine(disparar());
            disparando = true;

        }
    }

    public IEnumerator disparar()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            GameObject enemic = GameObject.FindWithTag("Duende");
            if (enemic != null && disparando)
            {
                GameObject proj = Instantiate(projectil);
                Vector2 direccio = enemic.transform.position - transform.parent.position;
                direccio.Normalize();
                proj.transform.up = direccio;
                proj.transform.position = this.transform.parent.position;
                proj.GetComponent<Rigidbody2D>().velocity = direccio * 10;
            }
            
        }
    }

}
