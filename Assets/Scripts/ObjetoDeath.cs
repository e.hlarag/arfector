using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoDeath : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] objetos;
    public GameObject recursos;
    void Start()
    {
        recursos.GetComponent<RecursosManager>().lvlup += escogeritem;
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void escogeritem()
    {
        Debug.Log("ITEM");
        this.gameObject.SetActive(true);
        int r = Random.Range(1, 4);
        int r2 = Random.Range(1, 4);
        int r3 = Random.Range(1, 4);
        if (r == 1)
        {
            GameObject objeto1 = Instantiate(objetos[0]);
            objeto1.transform.parent = this.transform;
            //objeto1.GetComponent<RectTransform>().
            objeto1.GetComponent<RectTransform>().offsetMin = new Vector2(28.61829f, 64.31205f);
            objeto1.GetComponent<RectTransform>().offsetMax = new Vector2(-868.6183f, -42.31205f);
            Debug.Log(objeto1 + " " + objeto1.transform.position);
            Debug.Log("ITEM1");
            
        }
        else if (r ==2)
        {
            GameObject objeto2 = Instantiate(objetos[1]);
            objeto2.transform.parent = this.transform;
            objeto2.GetComponent<RectTransform>().offsetMin= new Vector2(841.62f, 64.31f);
            objeto2.GetComponent<RectTransform>().offsetMax= new Vector2(-41.61996f, -42.31f);
            Debug.Log(objeto2 + " " + objeto2.transform.position);
            Debug.Log("ITEM2");

        }
        else if(r == 3)
        {
            GameObject objeto3 = Instantiate(objetos[2]);
            objeto3.transform.parent = this.transform;
            objeto3.GetComponent<RectTransform>().offsetMin = new Vector2(225.3645f, 65.646f);
            objeto3.GetComponent<RectTransform>().offsetMax = new Vector2(-222.9411f, -40.794f); Debug.Log(objeto3 + " " + objeto3.transform.position);
            Debug.Log("ITEM3");
        }
    }
}
