using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeSettings : MonoBehaviour
{
    public AudioMixer mixero;
    public Slider volumeSlider;
    public Slider volumeSlider2;

    public void ponehMusica()
    {
        float volum = volumeSlider.value;
        mixero.SetFloat("musica", Mathf.Log10(volum) * 20);
        float volum2 = volumeSlider2.value;
        mixero.SetFloat("sfx", Mathf.Log10(volum2) * 20);
    }
}
