using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : MonoBehaviour
{
    // Start is called before the first frame update
    public float atkspeed;
    public float range;
    public float dmg;
    public string type;
    public int stone;
    public int wood;
    
    public Torre(float atkspeed, float range, float dmg, string type, int stone, int wood)
    {
        this.type = type;
        this.atkspeed = atkspeed;
        this.range = range;
        this.stone = stone;
        this.wood = wood;
        this.dmg=dmg;
    }
    
    
    
    void Start()
    { 
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
