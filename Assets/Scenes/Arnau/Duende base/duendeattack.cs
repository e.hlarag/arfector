using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class duendeattack : MonoBehaviour
{
    public float rotationAmount = 70f;
    public float returnAmount = -70f;
    public float rotationSpeed = 100f;

  
    private bool giren = false;

    void Start()
    {
        StartCoroutine(ToggleBooleanCoroutine());
    }

    private IEnumerator ToggleBooleanCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            giren = !giren; 
        }
    }

    void Update()
    {

        if (giren)
        {

            float currentRotation = 0f;
            float rotationStep = rotationSpeed;
            transform.Rotate(Vector3.forward * rotationStep);
            currentRotation += rotationStep;
        }


        if (!giren)
        {
            float currentRotation = 0f;
            float rotationStep = rotationSpeed;
           
            transform.Rotate(Vector3.back * rotationStep);
            currentRotation -= rotationStep;
        }
    }
    
    
}